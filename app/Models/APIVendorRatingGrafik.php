<?php

namespace App\Models;

use CodeIgniter\Model;
use Config\Services;

class APIVendorRatingGrafik extends Model
{
    public function getItem($tahun, $bulan)
    {
        $client = Services::curlrequest();

        $url = "https://portal2.incoe.astra.co.id/vendor_rating/api/Grafik_by_tipe/" . $tahun . $bulan;

        $response = $client->request('GET', $url);

        $data = json_decode($response->getBody(), true);

        $filteredData = [];

        foreach ($data as $item) {
            if (
                strpos($item['part_number'], '-BUSH-') !== false ||
                strpos($item['part_number'], '-CONN-') !== false ||
                strpos($item['part_number'], '-LEST-') !== false ||
                strpos($item['part_number'], '-POLE-') !== false ||
                strpos($item['part_number'], '-TERE-') !== false ||
                strpos($item['part_number'], '-TERM-') !== false ||
                strpos($item['part_number'], '-PULE-') !== false ||
                strpos($item['part_number'], '-LEAL-') !== false ||
                strpos($item['part_number'], '-MOAL-') !== false 
            ) {
                $filteredData[] = $item;
            }
        }

        return $filteredData;
    }
}
