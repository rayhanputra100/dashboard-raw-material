<?= $this->extend('layouts/tamplate'); ?>

<?= $this->section('content'); ?>

<!-- Start Content-->
<h2 id="demotext" class="text-center mb-5">DASHBOARD LEAD RECEIPT</h2>
<div class="container-fluid bg-galaxy">
    <div class="row ">
        <div class="col-2 d-flex justify-content-center align-items-center mb-5">
            <div class="border-0 p-4 rounded-4 my-bg-dark">
                <form action="<?= site_url('/detailReceipt?date=' . $tanggal) ?>" method="get">
                    <h4 class="mb-4 text-white satoshi-bold">
                        <?php echo $tanggal == null ? date('M-Y') : date('M-Y', strtotime($tanggal)) ?>
                    </h4>
                    <label for="filter" class="form-label mb-3 satoshi-regular text-white">Filter</label>
                    <select class="form-control mb-2 satoshi-regular px-5" id="filter" name="tanggal">
                        <option value="<?php date('Y-m') ?>">-Default-</option>
                        <?php foreach ($bulan_tahun as $v) : ?>
                            <option value="<?= $v; ?>" <?= $tanggal === $v ? 'selected' : ''; ?>><?= date('M-Y', strtotime($v)); ?></option>
                        <?php endforeach ?>
                    </select>
                    <button class="btn btn-primary satoshi-regular" type="submit">Filter</button>
                </form>
            </div>
        </div>
        <div class="col-10 mb-5">
            <div class="border-0 p-2 rounded-4 my-bg-dark">
                <figure class="highcharts-figure">
                    <div id="containerReceive"></div>
            </div>
        </div>

        <div class="col-8 mb-5">
            <div class="border-0 p-2 rounded-4 my-bg-dark">
                <figure class="highcharts-figure">
                    <div id="containerVarian"></div>
                </figure>
            </div>
        </div><!-- end col-->
        <div class="col-4 mb-5">
            <div class="border-0 p-2 rounded-4 my-bg-dark">
                <figure class="highcharts-figure">
                    <div id="containerVarianBySup"></div>
                </figure>
            </div>
        </div><!-- end col-->
        <!-- <div class="col-12 ">

            <figure class="highcharts-figure">
                <div id="containerTruck"></div>
            </figure>


        </div> -->
        <!-- end col-->
    </div>

</div> <!-- container -->

<?= $this->endSection(); ?>