<?= $this->extend('layouts/tamplate'); ?>

<?= $this->section('content'); ?>


<!-- Start Content-->
<h2 id="demotext" class="text-center mb-5">DASHBOARD RAW MATERIAL</h2>
<div class="container-fluid bg-galaxy">
    <div class="row ">
        <div class="col-6 mb-5">
            <div class="border-0 p-2 rounded-4 my-bg-dark">
                <figure class="highcharts-figure">
                    <div id="">
                        <h3 class="text-white satoshi text-center">COMING SOON</h3>
                    </div>
            </div>
        </div>

        <div class="col-6 mb-5">
            <div class="border-0 p-2 rounded-4 my-bg-dark">
                <figure class="highcharts-figure">
                    <div id="containerRekap"></div>
                </figure>
            </div>
        </div><!-- end col-->
        <div class="col-6 mb-5">
            <div class="border-0 p-2 rounded-4 my-bg-dark">
                <figure class="highcharts-figure">
                    <div id="containerLavelStok">
                        <h3 class="text-white satoshi text-center">COMING SOON</h3>
                    </div>
                </figure>
            </div>
        </div><!-- end col-->
        <div class="col-6 ">
            <div class="border-0 p-2 rounded-4 my-bg-dark">
                <figure class="highcharts-figure">
                    <div id="">
                        <h3 class="text-white satoshi text-center">COMING SOON</h3>
                    </div>
            </div>
        </div>
        <!-- end col-->
    </div>

</div> <!-- container -->

<?= $this->endSection(); ?>