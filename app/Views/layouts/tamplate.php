<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8" />
    <title>Dashboard Raw Material</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="Warehouse Management System" name="Rayhan" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />

    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/bootstrap.min.css">

    <!-- My CSS -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/mycss.css">

    <!-- Icons -->
    <link rel="stylesheet" href="<?php echo base_url(); ?>assets/css/icons.css">

    <!-- App favicon -->
    <link rel="shortcut icon" href="<?= base_url(); ?>assets/images/iPTCBI.ico">


</head>

<!-- body start -->

<body class="bg-galaxy">

    <?= $this->include('/layouts/navbar'); ?>
    <br />
    <br />
    <br />
    <br />
    <?= $this->renderSection('content'); ?>
    <br />
    <!-- Footer Start -->
    <footer class="footer">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6 text-center text-white">
                    <script>
                        document.write(new Date().getFullYear())
                    </script> &copy; Dashboard Raw Material by <a href="">RayhanPJ</a>
                </div>
            </div>
        </div>
    </footer>
    <!-- end Footer -->

    <script src="<?php echo base_url(); ?>assets/js/bootstrap.min.js"></script>

    <script src="<?php echo base_url(); ?>assets/hightcharts/code/highcharts.js"></script>
    <script src="<?php echo base_url(); ?>assets/hightcharts/code/modules/exporting.js"></script>
    <script src="<?php echo base_url(); ?>assets/hightcharts/code/modules/export-data.js"></script>
    <script src="<?php echo base_url(); ?>assets/hightcharts/code/modules/accessibility.js"></script>


    <script type="text/javascript">
        Highcharts.chart("containerReceive", {
            chart: {
                type: "column",
                backgroundColor: "transparent",
            },
            title: {
                text: "LEAD RECEIPT",
                style: {
                    color: "white", // Ganti dengan warna teks yang diinginkan
                    fontFamily: "Satoshi-Bold, sans-serif",
                    textOutline: "none" // Menghilangkan border pada teks
                },
                align: "center",
            },
            xAxis: {
                categories: [
                    <?php foreach ($reportDN as $v) { ?>
                        <?php foreach ($v['y'] as $y) { ?> "<?php echo $y; ?>",
                        <?php } ?>
                    <?php } ?>
                ],
                labels: {
                    style: {
                        color: "white", // Ganti dengan warna teks yang diinginkan
                        fontFamily: "Satoshi-Bold, sans-serif",
                        textOutline: "none" // Menghilangkan border pada teks
                    },
                }
            },
            yAxis: [{
                min: <?php echo floatval(0); ?>,
                title: {
                    text: "Berat Supplier",
                    style: {
                        color: "white", // Ganti dengan warna teks yang diinginkan
                        fontFamily: "Satoshi-Bold, sans-serif",
                        textOutline: "none" // Menghilangkan border pada teks
                    },
                },
                stackLabels: {
                    enabled: true,
                },
                labels: {
                    style: {
                        color: "white", // Ganti dengan warna teks yang diinginkan
                        fontFamily: "Satoshi-Bold, sans-serif",
                        textOutline: "none" // Menghilangkan border pada teks
                    },
                }
            }],
            legend: {
                align: "left",
                x: 70,
                verticalAlign: "top",
                y: 30,
                floating: true,
                backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || "white",
                itemStyle: {
                    fontFamily: "Satoshi-Bold, sans-serif",
                },
                border: "none",
                shadow: false,
            },
            plotOptions: {
                column: {
                    stacking: "normal",
                    dataLabels: {
                        enabled: true,
                        style: {
                            fontFamily: "Satoshi-Bold, sans-serif",
                        },
                        formatter: function() {
                            if (this.y !== 0) {
                                return this.y + " kg";
                            }
                        },
                        inside: true,
                    },
                },
                spline: {
                    marker: {
                        enabled: true,
                    },
                    dataLabels: {
                        enabled: true,
                        style: {
                            fontFamily: "Satoshi-Bold, sans-serif",
                        },
                        formatter: function() {
                            if (this.y !== 0) {
                                return this.y + " kg";
                            }
                        },
                    },
                },
            },

            tooltip: {
                headerFormat: "<b>{point.x}</b><br/>",
                pointFormat: "{series.name}: {point.y:.2f} kg<br/>Total: {point.stackTotal:.2f} kg",
            },
            series: [{
                    name: "K-NFU",
                    color: {
                        linearGradient: {
                            x1: 0,
                            x2: 0,
                            y1: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.color("#08ff00").brighten(0.1).get()],
                            [1, Highcharts.color("#08ff00").brighten(0.4).get()]
                        ]
                    },
                    dataLabels: {
                        enabled: true,
                        style: {

                            fontFamily: "Satoshi-Bold, sans-serif",

                        },
                    },
                    data: [
                        <?php foreach ($reportDN as $v) { ?>
                            <?php foreach ($v['b'] as $b) { ?>
                                <?php echo floatval($b / 1000); ?>,
                            <?php } ?>
                        <?php } ?>
                    ],
                },
                {
                    name: "K-IML",
                    color: {
                        linearGradient: {
                            x1: 0,
                            x2: 0,
                            y1: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.color("#f84dff").brighten(0.1).get()],
                            [1, Highcharts.color("#f84dff").brighten(0.4).get()]
                        ]
                    },
                    dataLabels: {
                        enabled: true,
                        style: {

                            fontFamily: "Satoshi-Bold, sans-serif",

                        },
                    },
                    data: [
                        <?php foreach ($reportDN as $v) { ?>
                            <?php foreach ($v['c'] as $c) { ?>
                                <?php echo floatval($c / 1000 . " kg"); ?>,
                            <?php } ?>
                        <?php } ?>
                    ],
                },
                {
                    name: "Aktual K-NFU",
                    color: {
                        linearGradient: {
                            x1: 0,
                            x2: 0,
                            y1: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.color("#003007").brighten(0.1).get()],
                            [1, Highcharts.color("#003007").brighten(0.4).get()]
                        ]
                    },
                    stack: 1,
                    data: [
                        <?php foreach ($reportDN as $v) { ?>
                            <?php foreach ($v['e'] as $e) { ?>
                                <?php echo floatval($e . " kg"); ?>,
                            <?php } ?>
                        <?php } ?>
                    ],
                    dataLabels: {
                        enabled: true,
                        style: {

                            fontFamily: "Satoshi-Bold, sans-serif",

                        },
                    },
                },
                {
                    name: "Aktual K-IML",
                    color: {
                        linearGradient: {
                            x1: 0,
                            x2: 0,
                            y1: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.color("#520055").brighten(0.1).get()],
                            [1, Highcharts.color("#520055").brighten(0.4).get()]
                        ]
                    },
                    stack: 1,
                    data: [
                        <?php foreach ($reportDN as $v) { ?>
                            <?php foreach ($v['f'] as $f) { ?>
                                <?php echo floatval($f); ?>,
                            <?php } ?>
                        <?php } ?>
                    ],
                    dataLabels: {
                        enabled: true,
                        style: {

                            fontFamily: "Satoshi-Bold, sans-serif",

                        },
                    },
                },
                {
                    name: "Total Aktual",
                    stack: 2,
                    type: "spline",
                    data: [
                        <?php foreach ($reportDN as $v) { ?>
                            <?php foreach ($v['g'] as $g) { ?>
                                <?php echo floatval($g); ?>,
                            <?php } ?>
                        <?php } ?>
                    ],
                    dataLabels: {
                        enabled: true,
                        style: {

                            fontFamily: "Satoshi-Bold, sans-serif",

                        },
                    },
                },
            ],
        });
    </script>

    <script type="text/javascript">
        Highcharts.chart("containerRekap", {
            chart: {
                type: "column",
                backgroundColor: "transparent",
            },
            title: {
                text: "PLAN VS RECEIPT",
                style: {
                    color: "white", // Ganti dengan warna teks yang diinginkan
                    fontFamily: "Satoshi-Bold, sans-serif",
                    textOutline: "none" // Menghilangkan border pada teks
                },
                align: "center",
            },
            xAxis: {
                categories: [
                    <?php
                    $bulan = [];
                    for ($i = 1; $i <= 12; $i++) {
                        $bulan[] = date('F', mktime(0, 0, 0, $i, 1));
                    }
                    ?>
                    <?php foreach ($bulan as $v) { ?> "<?php echo $v; ?>",
                    <?php } ?>
                ],
                labels: {
                    style: {
                        color: "white", // Ganti dengan warna teks yang diinginkan
                        fontFamily: "Satoshi-Bold, sans-serif",
                        textOutline: "none" // Menghilangkan border pada teks
                    },
                }
            },
            yAxis: [{
                min: <?php echo floatval(0); ?>,
                title: {
                    text: "Total QTY",
                    style: {
                        color: "white", // Ganti dengan warna teks yang diinginkan
                        fontFamily: "Satoshi-Bold, sans-serif",
                        textOutline: "none" // Menghilangkan border pada teks
                    },
                },
                stackLabels: {
                    enabled: true,
                },
                labels: {
                    style: {
                        color: "white", // Ganti dengan warna teks yang diinginkan
                        fontFamily: "Satoshi-Bold, sans-serif",
                        textOutline: "none" // Menghilangkan border pada teks
                    },
                }
            }],
            legend: {
                align: "left",
                x: 70,
                verticalAlign: "top",
                y: 30,
                floating: true,
                backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || "white",
                itemStyle: {
                    fontFamily: "Satoshi-Bold, sans-serif",
                },
                border: "none",
                shadow: false,
            },
            tooltip: {
                headerFormat: "<b>{point.x}</b><br/>",
                pointFormat: "{series.name}: {point.y:.2f} kg<br/>Total: {point.stackTotal:.2f} kg",
            },
            plotOptions: {
                column: {
                    stacking: "normal",
                    dataLabels: {
                        enabled: true,
                        style: {
                            fontFamily: "Satoshi-Bold, sans-serif",
                        },
                        formatter: function() {
                            if (this.y !== 0) {
                                return this.y + " kg";
                            }
                        },
                        inside: true,
                    },
                },
                spline: {
                    marker: {
                        enabled: true,
                    },
                    dataLabels: {
                        enabled: true,
                        style: {
                            fontFamily: "Satoshi-Bold, sans-serif",
                        },
                        formatter: function() {
                            if (this.y !== 0) {
                                return this.y + " kg";
                            }
                        },
                    },
                },
            },
            series: [{
                    name: "Plan",
                    color: {
                        linearGradient: {
                            x1: 0,
                            x2: 0,
                            y1: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.color("#08ff00").brighten(0.1).get()],
                            [1, Highcharts.color("#08ff00").brighten(0.4).get()]
                        ]
                    },
                    dataLabels: {
                        enabled: true,
                        style: {

                            fontFamily: "Satoshi-Bold, sans-serif",

                        },
                    },
                    data: [
                        <?php foreach ($countAllPlan as $v) { ?>
                            <?php echo floatval($v); ?>,
                        <?php } ?>
                    ],
                },
                {
                    name: "Receipt",
                    stack: 1,
                    point: {
                        events: {
                            click: function() {
                                var category = this.category;
                                var monthIndex = this.series.xAxis.categories.indexOf(category);
                                var monthNumber = monthIndex + 1;
                                var year = 2023; // Ganti dengan tahun yang diinginkan

                                var date = year + "-" + ("0" + monthNumber).slice(-2);
                                var baseUrl = '<?php echo base_url(); ?>';
                                var pointUrl = 'detailReceipt?tanggal=' + date;
                                var fullUrl = baseUrl + pointUrl;
                                window.location.href = fullUrl;
                            }
                        }
                    },

                    color: {
                        linearGradient: {
                            x1: 0,
                            x2: 0,
                            y1: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.color("#ff00ef").brighten(0.1).get()],
                            [1, Highcharts.color("#ff00ef").brighten(0.4).get()]
                        ]
                    },
                    dataLabels: {
                        enabled: true,
                        style: {

                            fontFamily: "Satoshi-Bold, sans-serif",

                        },
                    },
                    data: [
                        <?php foreach ($countAllReceipt as $v) { ?>
                            <?php echo floatval($v); ?>,
                        <?php } ?>
                    ],
                },
                {
                    name: "Total QTY Plan",
                    stack: 2,
                    type: "spline",
                    data: [
                        <?php foreach ($countAllPlan as $v) { ?>
                            <?php echo floatval($v); ?>,
                        <?php } ?>
                    ],
                    dataLabels: {
                        enabled: true,
                        style: {

                            fontFamily: "Satoshi-Bold, sans-serif",

                        },
                    },
                },
                {
                    name: "Total QTY Receipt",
                    stack: 3,
                    type: "spline",
                    data: [
                        <?php foreach ($countAllReceipt as $v) { ?>
                            <?php echo floatval($v); ?>,
                        <?php } ?>
                    ],
                    dataLabels: {
                        enabled: true,
                        style: {

                            fontFamily: "Satoshi-Bold, sans-serif",

                        },
                    },
                },
            ],
        });
    </script>

    <script type="text/javascript">
        Highcharts.chart("containerVarian", {
            chart: {
                type: "column",
                backgroundColor: "transparent",
            },
            title: {
                text: "TOTAL VARIANCE",
                style: {
                    color: "white", // Ganti dengan warna teks yang diinginkan
                    fontFamily: "Satoshi-Bold, sans-serif",
                    textOutline: "none" // Menghilangkan border pada teks
                },
            },
            yAxis: [{
                title: {
                    text: "Variance",
                    style: {
                        color: "white", // Ganti dengan warna teks yang diinginkan
                        fontFamily: "Satoshi-Bold, sans-serif",
                        textOutline: "none" // Menghilangkan border pada teks
                    },
                },
                labels: {
                    style: {
                        color: "white", // Ganti dengan warna teks yang diinginkan
                        fontFamily: "Satoshi-Bold, sans-serif",
                        textOutline: "none" // Menghilangkan border pada teks
                    },
                }
            }],
            xAxis: [{
                categories: [
                    <?php foreach ($selisih as $v) { ?>
                        <?php foreach ($v['y'] as $y) { ?> "<?php echo $y; ?>",
                        <?php } ?>
                    <?php } ?>
                ],
                labels: {
                    style: {
                        color: "white", // Ganti dengan warna teks yang diinginkan
                        fontFamily: "Satoshi-Bold, sans-serif",
                        textOutline: "none" // Menghilangkan border pada teks
                    },
                }
            }, ],
            credits: {
                enabled: false,
            },
            legend: {
                backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || "white",
                itemStyle: {
                    fontFamily: "Satoshi-Bold, sans-serif",
                },

            },
            plotOptions: {
                column: {
                    borderRadius: "25%",
                    colorByPoint: true,
                    colors: [{
                        linearGradient: {
                            x1: 0,
                            x2: 0,
                            y1: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.color("#00FF00").brighten(0.1).get()], // Warna akhir (misal: merah tua)
                            [1, Highcharts.color("#00FF00").brighten(0.4).get()], // Warna tengah (misal: merah muda)
                        ],
                    }, ],
                    negativeColor: {
                        linearGradient: {
                            x1: 0,
                            x2: 0,
                            y1: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.color("#FF0000").brighten(0.4).get()], // Warna tengah (misal: merah muda)
                            [1, Highcharts.color("#FF0000").brighten(0.1).get()], // Warna akhir (misal: merah tua)
                        ],

                    },

                    tooltip: {
                        headerFormat: "<b>{point.x}</b><br/>",
                        pointFormat: "{series.name}: {point.y} kg",
                    },

                    dataLabels: {
                        enabled: true,
                        style: {
                            color: "white", // Ganti dengan warna teks yang diinginkan
                            fontFamily: "Satoshi-Bold, sans-serif",
                            textOutline: "none" // Menghilangkan border pada teks
                        },
                        formatter: function() {
                            var color =
                                this.y >= 0 ?
                                this.color :
                                Highcharts.color(this.series.options.negativeColor).brighten(0.5).get();
                            return (
                                '<span style="color:' +
                                color +
                                '">' +
                                this.y +
                                " kg" +
                                "</span>"
                            );
                        },
                    },
                },
            },
            series: [{
                    name: "K-IML Variance",
                    data: [
                        <?php foreach ($selisih as $v) { ?>
                            <?php foreach ($v['b'] as $b) { ?>
                                <?php echo floatval($b); ?>,
                            <?php } ?>
                        <?php } ?>
                    ],
                },
                {
                    name: "K-NFU Variance",
                    data: [
                        <?php foreach ($selisih as $v) { ?>
                            <?php foreach ($v['c'] as $c) { ?>
                                <?php echo floatval($c); ?>,
                            <?php } ?>
                        <?php } ?>
                    ],
                },

            ],
        });
    </script>

    <script type="text/javascript">
        Highcharts.chart("containerVarianBySup", {
            chart: {
                type: "column",
                backgroundColor: "transparent",
            },
            title: {
                text: "VARIANCE BY SUPPLIER",
                style: {
                    color: "white", // Ganti dengan warna teks yang diinginkan
                    fontFamily: "Satoshi-Bold, sans-serif",
                    textOutline: "none" // Menghilangkan border pada teks
                },
            },
            yAxis: [{

                title: {
                    text: "Varian Supplier",
                    style: {
                        color: "white", // Ganti dengan warna teks yang diinginkan
                        fontFamily: "Satoshi-Bold, sans-serif",
                        textOutline: "none" // Menghilangkan border pada teks
                    },
                },
                labels: {
                    style: {
                        color: "white", // Ganti dengan warna teks yang diinginkan
                        fontFamily: "Satoshi-Bold, sans-serif",
                        textOutline: "none" // Menghilangkan border pada teks
                    },
                }
            }],
            xAxis: {
                categories: ["<?= $tanggal ?>"],
                labels: {
                    style: {
                        color: "white", // Ganti dengan warna teks yang diinginkan
                        fontFamily: "Satoshi-Bold, sans-serif",
                        textOutline: "none" // Menghilangkan border pada teks
                    },
                }
            },
            credits: {
                enabled: false,
            },
            legend: {
                backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || "white",
                itemStyle: {
                    fontFamily: "Satoshi-Bold, sans-serif",
                },

            },
            plotOptions: {
                column: {
                    borderRadius: "25%",
                    colorByPoint: true,
                    colors: [{
                        linearGradient: {
                            x1: 0,
                            x2: 0,
                            y1: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.color("#00FF00").brighten(0.1).get()], // Warna akhir (misal: merah tua)
                            [1, Highcharts.color("#00FF00").brighten(0.4).get()], // Warna tengah (misal: merah muda)
                        ],
                    }, ],
                    negativeColor: {
                        linearGradient: {
                            x1: 0,
                            x2: 0,
                            y1: 0,
                            y2: 1
                        },
                        stops: [
                            [0, Highcharts.color("#FF0000").brighten(0.4).get()], // Warna tengah (misal: merah muda)
                            [1, Highcharts.color("#FF0000").brighten(0.1).get()], // Warna akhir (misal: merah tua)
                        ],
                    },

                    tooltip: {
                        headerFormat: "<b>{point.x}</b><br/>",
                        pointFormat: "{series.name}: {point.y} kg",
                    },

                    dataLabels: {
                        enabled: true,

                        style: {
                            color: "white", // Ganti dengan warna teks yang diinginkan
                            fontFamily: "Satoshi-Bold, sans-serif",
                            textOutline: "none" // Menghilangkan border pada teks
                        },

                        formatter: function() {
                            var color =
                                this.y >= 0 ?
                                this.color :
                                Highcharts.color(this.series.options.negativeColor).brighten(0.5).get();
                            return (
                                '<span style="color:' +
                                color +
                                '">' +
                                this.y +
                                " kg" +
                                "</span>"
                            );
                        },
                    },
                },
            },
            series: [{
                    name: "K-NFU",
                    data: [
                        <?php foreach ($selisihBySup as $v) { ?>
                            <?php echo isset($v['b'][1]) ? floatval($v['b'][1]) : 0; ?>,
                        <?php } ?>
                    ],
                },
                {
                    name: "K-IML",
                    data: [
                        <?php foreach ($selisihBySup as $v) { ?>
                            <?php echo isset($v['c'][2]) ? floatval($v['c'][2]) : 0; ?>,
                        <?php } ?>
                    ],
                }
            ],
        });
    </script>

    <script type="text/javascript">
        Highcharts.chart("containerTruck", {
            chart: {
                type: "column",
                backgroundColor: "transparent",
            },
            title: {
                text: "QTY TRUCK",
                align: "center",
            },
            xAxis: {
                categories: [
                    <?php foreach ($reportDN as $v) { ?>
                        <?php foreach ($v['y'] as $y) { ?> "<?php echo $y; ?>",
                        <?php } ?>
                    <?php } ?>
                ],
            },
            yAxis: {
                min: 0,
                title: {
                    text: "Count trophies",
                },
                stackLabels: {
                    enabled: true,
                },
            },
            legend: {
                align: "left",
                x: 70,
                verticalAlign: "top",
                y: 70,
                floating: true,
                backgroundColor: Highcharts.defaultOptions.legend.backgroundColor || "white",
                borderColor: "#CCC",
                borderWidth: 1,
                shadow: false,
            },
            tooltip: {
                headerFormat: "<b>{point.x}</b><br/>",
                pointFormat: "{series.name}: {point.y} kg<br/>Total: {point.stackTotal}",
            },
            plotOptions: {
                column: {
                    stacking: "normal",
                    dataLabels: {
                        enabled: true,
                    },
                },
            },
            series: [{
                    name: "K-NFU",
                    data: [
                        <?php foreach ($reportDN as $v) { ?>
                            <?php foreach ($v['b'] as $b) { ?>
                                <?php echo floatval($b); ?>,
                            <?php } ?>
                        <?php } ?>
                    ],
                },
                {
                    name: "K-IML",
                    data: [
                        <?php foreach ($reportDN as $v) { ?>
                            <?php foreach ($v['c'] as $c) { ?>
                                <?php echo floatval($c); ?>,
                            <?php } ?>
                        <?php } ?>
                    ],
                },
            ],
        });
    </script>



</body>

</html>