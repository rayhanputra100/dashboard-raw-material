<nav class="navbar navbar-example navbar-expand-lg bg-light nav-blur bg-transparent fixed-top">
    <div class="container-fluid d-flex align-items-center justify-content-center">
        <a class="navbar-brand" href="/">
            <img src="<?php echo base_url(); ?>assets/images/PTCBI.png" alt="CBI" width="46" height="39">
            <span class="navbar-brand satoshi">PT. CBI</span>
        </a>

        <div class="collapse navbar-collapse justify-content-center flex-grow-1" id="navbar-ex-3">
            <div class="navbar-nav">
                <a class="nav-item nav-link active text-white satoshi-bold nav-transition" href="<?php echo base_url() ?>">Home</a>
            </div>
        </div>
    </div>
</nav>
