<?php

namespace App\Controllers;

use App\Models\APIVendorRatingGrafik;
use App\Models\TArea;
use App\Models\APIVendorRating;
use App\Models\APIReportDN;
class Home extends BaseController
{
    protected $apiModelVRG;
    protected $areaModel;
    protected $apiModelVR;
    protected $apiModelRDN;
    public function __construct()
    {
        $this->apiModelVRG = new APIVendorRatingGrafik();
        $this->areaModel = new TArea();
        $this->apiModelVR = new APIVendorRating();
        $this->apiModelRDN = new APIReportDN();
    }

    
    private function selisih()
    {
        $tanggal = $this->request->getGet('tanggal');
        $f = $this->apiModelRDN->getItem($tanggal);
        $uniqueTanggal = [];
        $uniqueDate = [];
        $selisihIMPORT = [];
        $selisihKNFU = [];
        $selisihKIML = [];
        $totalSelisihHarian = [];

        foreach ($f as $item) {
            $date = $item['tanggal'];
            $formattedDate = date('d', strtotime($date));
            if (!in_array($formattedDate, $uniqueDate)) {
                $uniqueDate[] = $formattedDate;
            }
        }

        foreach ($f as $item) {
            $date = $item['tanggal'];
            if (!in_array($date, $uniqueTanggal)) {
                $uniqueTanggal[] = $date;
            }
        }

        foreach ($uniqueTanggal as $tanggal) {
            $totalSelisihIMPORT = 0;
            $totalSelisihKNFU = 0;
            $totalSelisihKIML = 0;

            foreach ($f as $item) {
                if ($item['tanggal'] == $tanggal) {
                    if ($item["supplier"] == "IMPORT") {
                        $totalSelisihIMPORT += $item["selisih"];
                    } elseif ($item["supplier"] == "K-NFU") {
                        $totalSelisihKNFU += $item["selisih"];
                    } elseif ($item["supplier"] == "K-IML") {
                        $totalSelisihKIML += $item["selisih"];
                    }
                }
            }

            $selisihKIML[$tanggal] = $totalSelisihKIML;
            $selisihKNFU[$tanggal] = $totalSelisihKNFU;
        }

        $jsonData = [
            [
                "y" => $uniqueDate,
                "a" => count($selisihKNFU) + count($selisihKIML),
                "b" => array_values($selisihKIML),
                "c" => array_values($selisihKNFU),
            ]
        ];

        return $jsonData;
    }
    private function selisihBySup()
    {
        $tanggal = $this->request->getGet('tanggal');
        $f = $this->apiModelRDN->getItem($tanggal);
        $uniqueSupplier = [];
        $selisihIMPORT = [];
        $selisihKNFU = [];
        $selisihKIML = [];

        foreach ($f as $item) {
            $supplier = $item['supplier'];
            if (!in_array($supplier, $uniqueSupplier)) {
                $uniqueSupplier[] = $supplier;
            }
        }

        foreach ($uniqueSupplier as $supplier) {
            $totalSelisihIMPORT = 0;
            $totalSelisihKNFU = 0;
            $totalSelisihKIML = 0;

            foreach ($f as $item) {
                if ($item['supplier'] == $supplier) {
                    if ($item["supplier"] == "IMPORT") {
                        $totalSelisihIMPORT += $item["selisih"];
                    } elseif ($item["supplier"] == "K-NFU") {
                        $totalSelisihKNFU += $item["selisih"];
                    } elseif ($item["supplier"] == "K-IML") {
                        $totalSelisihKIML += $item["selisih"];
                    }
                }
            }

            $selisihIMPORT[$supplier] = $totalSelisihIMPORT;
            $selisihKNFU[$supplier] = $totalSelisihKNFU;
            $selisihKIML[$supplier] = $totalSelisihKIML;
        }

        $jsonData = [
            [
                "y" => $uniqueSupplier,
                "a" => array_values($selisihIMPORT),
                "b" => array_values($selisihKNFU),
                "c" => array_values($selisihKIML),
            ]
        ];

        return $jsonData;
    }
    private function reportDN()
    {
        $tanggal = $this->request->getGet('tanggal');
        $f = $this->apiModelRDN->getItem($tanggal);
        $uniqueTanggal = [];
        $uniqueDate = [];
        $BeratIMPORT = [];
        $BeratKNFU = [];
        $BeratKIML = [];
        $AktualIMPORT = [];
        $AktualKNFU = [];
        $AktualKIML = [];
        $AktualTotal = [];

        foreach ($f as $item) {
            $date = $item['tanggal'];
            $formattedDate = date('d', strtotime($date));
            if (!in_array($formattedDate, $uniqueDate)) {
                $uniqueDate[] = $formattedDate;
            }
        }

        foreach ($f as $item) {
            $date = $item['tanggal'];
            if (!in_array($date, $uniqueTanggal)) {
                $uniqueTanggal[] = $date;
            }
        }


        foreach ($uniqueTanggal as $tanggal) {
            $totalBeratIMPORT = 0;
            $totalBeratKNFU = 0;
            $totalBeratKIML = 0;
            $totalBeratAktualIMPORT = 0;
            $totalBeratAktualKNFU = 0;
            $totalBeratAktualKIML = 0;

            foreach ($f as $item) {
                if ($item['tanggal'] == $tanggal) {
                    if ($item["supplier"] == "IMPORT") {
                        $totalBeratIMPORT += intval($item["berat_supplier"]);
                    } elseif ($item["supplier"] == "K-NFU") {
                        $totalBeratKNFU += intval($item["berat_supplier"]);
                        $totalBeratAktualKNFU += floatval($item["berat_aktual"]);
                    } elseif ($item["supplier"] == "K-IML") {
                        $totalBeratKIML += intval($item["berat_supplier"]);
                        $totalBeratAktualKIML += floatval($item["berat_aktual"]);
                    }
                }
            }


            $BeratIMPORT[$tanggal] = $totalBeratIMPORT;
            $BeratKNFU[$tanggal] = $totalBeratKNFU;
            $BeratKIML[$tanggal] = $totalBeratKIML;
            $AktualIMPORT[$tanggal] = $totalBeratAktualIMPORT;
            $AktualKNFU[$tanggal] = $totalBeratAktualKNFU;
            $AktualKIML[$tanggal] = $totalBeratAktualKIML;
            $AktualTotal[$tanggal] = $totalBeratAktualKIML + $totalBeratAktualKNFU;
        }

        $jsonData = [
            [
                "y" => $uniqueDate,
                "a" => array_values($BeratIMPORT),
                "b" => array_values($BeratKNFU),
                "c" => array_values($BeratKIML),
                "d" => array_values($AktualIMPORT),
                "e" => array_values($AktualKNFU),
                "f" => array_values($AktualKIML),
                "g" => array_values($AktualTotal),
            ]
        ];

        return $jsonData;
    }

    private function getItemsByMonth($bulan)
    {
        $tahun = $this->request->getGet('tahun') == null ? date('Y') : $this->request->getGet('tahun');

        $url = $this->apiModelVRG->getItem($tahun, $bulan);

        return $url;
    }

    private function countAllPlan()
    {
        $countAllPlan = [
            'Jan' => 0,
            'Feb' => 0,
            'Mar' => 0,
            'Apr' => 0,
            'Mei' => 0,
            'Jun' => 0,
            'Jul' => 0,
            'Agu' => 0,
            'Sept' => 0,
            'Oct' => 0,
            'Nov' => 0,
            'Dec' => 0,
        ];

        $months = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sept', 'Oct', 'Nov', 'Dec'];

        foreach ($months as $month) {
            $data = $this->getItemsByMonth(str_pad(array_search($month, $months) + 1, 2, '0', STR_PAD_LEFT));

            foreach ($data as $v) {
                $qtyPlan = $v['qty_plan'];
                if ($qtyPlan != 0) {
                    $countAllPlan[$month] += $qtyPlan;
                }
            }
        }

        return $countAllPlan;
    }

    private function countAllReceipt()
    {
        $countAllReceipt = [
            'Jan' => 0,
            'Feb' => 0,
            'Mar' => 0,
            'Apr' => 0,
            'Mei' => 0,
            'Jun' => 0,
            'Jul' => 0,
            'Agu' => 0,
            'Sept' => 0,
            'Oct' => 0,
            'Nov' => 0,
            'Dec' => 0,
        ];

        $months = ['Jan', 'Feb', 'Mar', 'Apr', 'Mei', 'Jun', 'Jul', 'Agu', 'Sept', 'Oct', 'Nov', 'Dec'];

        foreach ($months as $month) {
            $data = $this->getItemsByMonth(str_pad(array_search($month, $months) + 1, 2, '0', STR_PAD_LEFT));

            foreach ($data as $v) {
                $qtyReceipt = $v['receipt_qty'];
                if ($qtyReceipt != 0) {
                    $countAllReceipt[$month] += $qtyReceipt;
                }
            }
        }

        return $countAllReceipt;
    }

    public function index()
    {
        $reportDN = $this->reportDN();
        $selisih = $this->selisih();
        $selisihBySup = $this->selisihBySup();
        $tanggal = date('d');
        $bulan_tahun = [];
        $bulan = [];
        for ($i = 1; $i <= 12; $i++) {
            $bulan[] = date('F', mktime(0, 0, 0, $i, 1));
        }
        for ($bulan = 1; $bulan <= 12; $bulan++) {
            $tahun = date('Y'); // Get the current year
            $bulan_formatted = sprintf("%02d", $bulan); // Convert the month number to a 2-digit format with leading zeros
            $bulan_tahun[] = $tahun . "-" . $bulan_formatted;
        }
        $countAllPlan = $this->countAllPlan();
        $countAllReceipt = $this->countAllReceipt();

        $datas = [
            'countAllPlan' => $countAllPlan,
            'countAllReceipt' => $countAllReceipt,
            'tanggal' => $tanggal,
            'bulan' => $bulan,
            'bulan_tahun' => $bulan_tahun,
            'reportDN' => $reportDN,
            'selisih' => $selisih,
            'selisihBySup' => $selisihBySup
        ];

        return view('pages/home', $datas);
    }
}
